#include <stdio.h>
#include <stdlib.h>

#define SIM "SIM\n"
#define NAO "NÃO\n"

void mergeSort( int *vetorDesorndeado, int posicaoInicio, int posicaoFim );

int main()
{
	int p1[5000], p2[5000], p3[5000];
	int n, x, i, j, k, flag = 0;

	scanf("%d %d",&n,&x);
	while(n != 0 && x!= 0)
	{
		
		
		for(i = 0;i < n; i++)
			scanf("%d",&p1[i]);

		for(i = 0;i < n; i++)
			scanf("%d",&p2[i]);

		for(i = 0;i < n; i++)
			scanf("%d",&p3[i]);

		//Ordena os 3 vetores
		mergeSort(p1,0,n-1);
		mergeSort(p2,0,n-1);
		mergeSort(p3,0,n-1);

		//Faz a busca dentro de cada vetor
		for(i = 0; i < n && flag != 1; i++)
		{
			if(p1[i] + p2[0] + p3[0] > x || p1[i] + p2[n-1] + p3[n-1] < x)
					continue;
			else
			{
				for(j = 0; j < n && flag != 1; j++)
				{
					if(p1[i] + p2[j]+ p3[0] > x || p1[i] + p2[j] + p3[n-1] < x)
						continue;
					else
					{
						for(k = 0; k < n && flag != 1; k++)
						{
							if(p1[i] + p2[j] + p3[k] == x)
							{
								flag = 1;
								//return;
							}
							else
								if(p1[i] + p2[j] + p3[k] > x)//se passou o valor procurado incrementa uma posição no vetor anterior(pois esta em ordem crescente)
									break;
						}
					}
				}
			}
		}

		if(flag == 1)
			printf(SIM);
		else
			printf(NAO);
		flag = 0;
		scanf("%d %d",&n,&x);
	}

	return 0;
}

//Merge Sort para ordenar os vetores
void mergeSort( int *vetorDesorndeado, int posicaoInicio, int posicaoFim ) 
{
   int i,j,k,metadeTamanho,*vetorTemp;
   if (posicaoInicio == posicaoFim) 
   		return;
   
   // ordenacao recursiva das duas metades
   metadeTamanho = (posicaoInicio+posicaoFim)/2;
   mergeSort (vetorDesorndeado, posicaoInicio, metadeTamanho);
   mergeSort (vetorDesorndeado, metadeTamanho+1, posicaoFim);

   // intercalacao no vetor temporario t
   i = posicaoInicio;
   j = metadeTamanho+1;
   k = 0;
   vetorTemp = (int *) malloc(sizeof(int) * (posicaoFim-posicaoInicio+1));
   
   while(i < metadeTamanho+1 || j  < posicaoFim+1)
   { 
      if (i == metadeTamanho+1)
      { // i passou do final da primeira metade, pegar v[j]
         vetorTemp[k] = vetorDesorndeado[j];
         j++;
         k++;
      } 
      else
      {
         if (j == posicaoFim+1) 
         { 
            // j passou do final da segunda metade, pegar v[i]
            vetorTemp[k] = vetorDesorndeado[i];
            i++;
            k++;
         } 
         else 
         {
            if (vetorDesorndeado[i] < vetorDesorndeado[j]) 
            { 
               vetorTemp[k] = vetorDesorndeado[i];
               i++;
               k++;
            } 
            else
            { 
              vetorTemp[k] = vetorDesorndeado[j];
              j++;
              k++;
            }
         }
      }
      
   }
   // copia vetor intercalado para o vetor original
   for(i = posicaoInicio; i <= posicaoFim; i++)
   		vetorDesorndeado[i] = vetorTemp[i-posicaoInicio];
   free(vetorTemp);
}
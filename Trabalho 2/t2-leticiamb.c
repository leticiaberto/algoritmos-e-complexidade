#include <stdio.h>
#include <stdlib.h>

#define MAX 5000

int matriz[MAX][MAX];

int main()
{

	int n,i,j;
	
	int tamMatriz = 0;
	int aux1,aux2;
	scanf("%d",&n);
	
	while(n != 0)
	{
		//Como os dados ficarão na forma triangular trabalhei com as condições de parada abaixo. As demais posições da matriz nao recebem valores digitados pelo usuario
		for(i = 1; i <= n; i++)//usei indexação começando de 1 para facilitar os cálculos
			for (j = 1; j <= i; j++)
				scanf("%d",&matriz[i][j]);

		
		for(i = n-1; i >=1 ; i--)
		{
			j = 1;
			while(j <= i)//começa da penultima linha e soma cada posição com o valor de baixo e da diagonal debaixo. O maior valor é substituido na matriz original e usado ate chegar na primeira posição. Ao final teremos o maior caminho na primeira posição
			{
				aux1 = matriz[i][j] + matriz[i+1][j];
				aux2 = matriz[i][j] + matriz[i+1][j+1];
				if(aux1 > aux2)
					matriz[i][j] = aux1;
				else
					matriz[i][j] = aux2;
				j++;
			}
		}
		printf("%d\n",matriz[1][1]);

		scanf("%d",&n);
	}
	return 0;
}

#include <stdio.h>

#define MAX 1000000
int main()
{
	int tam, soma, v[MAX], i, aux;
	soma = 0;
		
	scanf("%d", &tam);
	while(tam != 0)
	{
		for(i = 0; i < tam; i++)
		{
			scanf("%d",&v[i]);
		}
	
		aux = 0;
		for(i = 0; i < tam; i++)
		{
			aux = aux + v[i];
			if(aux < 0)//zerar a soma maxima anterior e começar uma nova sequencia
				aux = 0;
			else 
				if(aux > soma)
					soma = aux;
		}
		printf("%d\n",soma);
		soma = 0;
		scanf("%d", &tam);
	}
	return 0;
}
